function docker_image
{
    PROJECT=$1
    NAME=$2
    docker buildx build -t "$NAME" "$PROJECT"
}

function maven_spring_native
{
    cd $1 && ./mvnw clean package -DskipTests -Pnative native:compile
    cd .. && docker_image $1 $2
}

function maven_quarkus_native
{
    cd $1 && ./mvnw clean package -DskipTests -Dnative
    cd .. && docker_image $1 $2
}

function angular
{
    docker_image $1 $2
}

mkdir temp && cd temp

git clone https://gitlab.com/catalogar-videos/components/portal-catalogo-video.git
git clone https://gitlab.com/catalogar-videos/components/catalogo-video.git
git clone https://gitlab.com/catalogar-videos/components/captura-eventos-quarkus.git
git clone https://gitlab.com/catalogar-videos/components/captura-video-spring.git

angular "portal-catalogo-video" "portal"
maven_spring_native catalogo-video catalogo-video
maven_quarkus_native captura-eventos-quarkus captura-eventos
maven_spring_native captura-video-spring captura-video