db.createCollection("video_source");

db.createCollection("videos");

db.video_source.insertMany
(
  [
    {
      "alias": "N",
      "name": "NOME_VIDEO_SOURCE",
      "required_parameters": ["url", "author", "tags", "createDate"],
      "templates": ["https://www.NOME_VIDEO_SOURCE.com/videos/hd/{id}.mp4", "https://www.NOME_VIDEO_SOURCE.com/videos/{id}_720p.mp4"],
      "type": "link"
    },
    {
      "alias": "SPA",
      "name": "NOME_VIDEO_SOURCE_2",
      "required_parameters": ["url", "author", "tags", "createDate"],
      "templates": "[]",
      "type": "link"
    }
  ]
);