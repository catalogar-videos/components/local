#!/bin/bash

# Definição das variáveis de ambiente
EXCHANGE="topic.captura.event"
QUEUE_CAPTURA_INPUT="queue.captura.input"
QUEUE_CAPTURA_OUTPUT="queue.captura.output"
QUEUE_CAPTURA_EVENT="queue.captura.event"

call_rabbitmqadmin() {
  rabbitmqadmin -u "$RABBITMQ_USER" -p "$RABBITMQ_PASSWORD" -H "$RABBITMQ_HOST" -P "$RABBITMQ_HTTP_PORT" --vhost="$RABBITMQ_USER" "$@"
}

function rabbitmq() {

  call_rabbitmqadmin declare vhost name="$RABBITMQ_USER"

  # Create the queues
  call_rabbitmqadmin declare queue name="$QUEUE_CAPTURA_INPUT" durable=true
  call_rabbitmqadmin declare queue name="$QUEUE_CAPTURA_OUTPUT" durable=true
  call_rabbitmqadmin declare queue name="$QUEUE_CAPTURA_EVENT" durable=true

  # Create the exchange
  call_rabbitmqadmin declare exchange name="$EXCHANGE" type=topic durable=true

  # Create the bindings
  call_rabbitmqadmin declare binding destination="$QUEUE_CAPTURA_OUTPUT" source="$EXCHANGE" routing_key="output"
  call_rabbitmqadmin declare binding destination="$QUEUE_CAPTURA_EVENT" source="$EXCHANGE" routing_key="#"

}

rabbitmq